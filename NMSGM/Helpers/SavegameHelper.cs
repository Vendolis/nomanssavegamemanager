﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMSGM.Helpers
{
    public static class SavegameHelper
    {
        public static uint GetProfileFromFilename(string filename)
        {
            switch (filename)
            {
                case "save.hg":
                    return 0;
                case "save2.hg":
                    return 1;
                case "save3.hg":
                    return 2;
                case "save4.hg":
                    return 3;
                case "save5.hg":
                    return 4;
                case "save6.hg":
                    return 5;
                case "save7.hg":
                    return 6;
                case "save8.hg":
                    return 7;
                case "save9.hg":
                    return 8;
                case "save10.hg":
                    return 9;
                default:
                    throw new InvalidDataException();
            }
        }

        public static string GetStorageFilenameFromProfileId(uint id)
        {
            switch (id)
            {
                case 0:
                    return "save.hg";
                case 1:
                    return "save2.hg";
                case 2:
                    return "save3.hg";
                case 3:
                    return "save4.hg";
                case 4:
                    return "save5.hg";
                case 5:
                    return "save6.hg";
                case 6:
                    return "save7.hg";
                case 7:
                    return "save8.hg";
                case 8:
                    return "save9.hg";
                case 9:
                    return "save10.hg";
                default:
                    throw new InvalidDataException();
            }
        }
    }
}
