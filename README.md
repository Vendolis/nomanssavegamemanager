# README #

---
## Update regarding NEXT Update ##

With the changes in the NEXT update the original version of NMSGM stoped working. I created a Fork of the Original Version from *leapfrog* to fix the issues. *Leapfrog* is currently not able to update the original. For now I will continue to support this fork until the original version has been brought up to date.

## Binary release ##

Please find the binary release of the project at: https://nmsgm.blob.core.windows.net/nmsgmrelease/publish/publish.htm

**!!! This will throw security warnings, since I have no code-signing certificate. If you want to avoid this, you will have to build the tool from the source. !!!**

---

## Project information ##

This is the sourcecode running behind No Man's SaveGame Manager made available for everyone to look at, modify or use for their own purposes.

Details on this can be found on these links:

* https://nomansskymods.com/mods/no-mans-savegame-manager/
* https://www.reddit.com/r/NoMansSkyTheGame/comments/4yqknr/no_mans_savegame_manager_nmsgm_beta_release/

## Building ##
The source code should build fine if used in VS2017 having nuget enabled.

## License ##
Copyright (c) 2016 leepfrog

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.